from typing import NoReturn

from telegram import Update
from telegram.ext import CallbackContext

from db.operate import add_picture


def conversation_router_picture(update: Update, context: CallbackContext) -> NoReturn:
    """
    Function runs when user uploads an image on Telegram bot
    :param update: Update object
    :param context: CallbackContext object
    :return: None
    """
    user_id = update.effective_user.id
    picture_id = update.message.photo[-1].file_id
    message_id = update.message.message_id
    res = add_picture(
        picture_id=picture_id,
        user_id=user_id,
        message_id=message_id
    )

    if res is False:
        update.message.delete()
