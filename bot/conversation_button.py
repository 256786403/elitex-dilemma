from typing import (
    NoReturn,
    AnyStr
)

from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CallbackContext

from bot.question_sender import questions_process
from bot.result_sender import get_result
from db.operate import (
    create_new_user,
    save_result_to_db,
    assign_group_activity_number,
)

state = {}


def conversation_router_button(update: Update, context: CallbackContext) -> NoReturn:
    """
    Function runs when user clicks any button on Telegram
    :param update: Update object
    :param context: CallbackContext object
    :return: None
    """
    message = update.callback_query.data
    if message == 'check_new_questions_exist':
        questions_process(
            update=update,
            context=context
        )
        update.callback_query.message.delete()

    elif message == 'get_result':
        get_result(update=update)
        questions_process(update, context)
        update.callback_query.message.delete()

    elif message.startswith('answer_'):
        save_answer(
            update=update,
            message=message,
            context=context
        )
        questions_process(update, context)

    elif message.startswith('back_to_group_'):
        splitted_message = message.split('#')
        back_to_group(
            update=update,
            context=context,
            group_id=splitted_message[-1]
        )
        update.callback_query.edit_message_text(
            f'You\'ve switched to group "{splitted_message[-1]}"'
        )
    elif message == 'no_action':
        pass
    else:
        actions = {
            'create_new_group': create_new_group,
            'join_new_group': join_new_group,
            'join_default_group': join_default_group,
        }
        action = actions.get(message)
        if action is not None:
            update.callback_query.message.delete()
            action(update=update, context=context)


def save_answer(update: Update, message: AnyStr, context: CallbackContext) -> NoReturn:
    """
    Parses users answer in order to save to database
    :param update: Update object
    :param message: string
    :param context: CallbackContext object
    :return: None
    """
    user_id = update.effective_user.id
    _, question_id, answer = message.split('_')
    save_result_to_db(
        user_id=user_id,
        question_id=question_id,
        answer=int(answer)
    )

    selected_keyboard = []
    inline_keyboard = update.callback_query.message.reply_markup.inline_keyboard
    for line in inline_keyboard:
        for item in line:
            if item.callback_data == message:
                selected_button = InlineKeyboardButton(item.text, callback_data='no_action')
                selected_keyboard.append([selected_button])

    context.bot.delete_message(
        chat_id=update.effective_chat.id,
        message_id=update.effective_message.message_id,
    )

    context.bot.sendPhoto(
        chat_id=update.callback_query.message.chat.id,
        photo=update.callback_query.message.photo[-1],
        reply_markup=InlineKeyboardMarkup(selected_keyboard)
    )


def create_new_group(update: Update, context: CallbackContext) -> NoReturn:
    update.callback_query.message.reply_text('Please, enter new group name')
    state[update.effective_chat.id] = 'add_new_group_user'


def join_new_group(update: Update, context: CallbackContext) -> NoReturn:
    update.callback_query.message.reply_text('Please, enter the name of group to join')
    state[update.effective_chat.id] = 'join_new_group_user'


def join_default_group(update: Update, context: CallbackContext) -> NoReturn:
    """
    Creates new user and assigns to them to default group
    :param update: Update object
    :param context: CallbackContext object
    :return:
    """
    chat_user = update.effective_chat
    new_user_data = {
        'user_id': chat_user.id,
        'username': chat_user.username,
        'first_name': chat_user.first_name,
        'last_name': chat_user.last_name,
        'role': 'admin',
        'group_id': 'default'
    }
    create_new_user(user_data=new_user_data)
    activity_number = update.callback_query.message.message_id  # the highest number has the last active group
    user_id = update.effective_user.id
    assign_group_activity_number(
        user_id=user_id,
        activity_number=activity_number,
        group_id='default'
    )

    update.callback_query.message.reply_text(
        f'You\'ve joined to  Default group'
    )


def back_to_group(update: Update, context: CallbackContext, group_id: int) -> NoReturn:
    """
    Assigns user back to group
    :param update: Update object
    :param context: CallbackContext
    :param group_id: integer
    :return: None
    """
    activity_number = update.callback_query.message.message_id  # the highest number has the last active group
    user_id = update.effective_user.id
    assign_group_activity_number(
        user_id=user_id,
        activity_number=activity_number,
        group_id=group_id
    )
    questions_process(
        update=update,
        context=context
    )
