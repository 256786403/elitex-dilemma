from typing import NoReturn

from telegram import Update
from telegram.ext import CallbackContext

from bot.new_user_checker import new_user_group_choice
from bot.question_sender import questions_process


def start(update: Update, context: CallbackContext) -> NoReturn:
    """
    Function runs when user enters the Telegram bot
    :param update: Update object
    :param context: CallbackContext object
    :return: None
    """
    update.message.reply_text(f'Hello {update.effective_user.first_name}!')
    new_user_group_choice(update=update)
    update.message.delete()


def continue_command(update: Update, context: CallbackContext):
    """
    Function runs when user enters the command /continue on Telegram bot
    :param update: Update object
    :param context: CallbackContext object
    :return: None
    """
    questions_process(update=update, context=context)
    update.message.delete()
