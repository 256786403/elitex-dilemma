from typing import NoReturn, AnyStr

from telegram import Update
from telegram.ext import CallbackContext

from bot.conversation_button import questions_process, state
from db.operate import (
    create_new_user,
    create_group,
    get_group_by_name,
    assign_group_activity_number,
    delete_picture,
    change_user_role
)


def conversation_router_text(update: Update, context: CallbackContext) -> NoReturn:
    """
    Function runs when user enters any text message on Telegram bot
    :param update: Update object
    :param context: CallbackContext object
    :return: None
    """
    user_id = update.effective_chat.id
    message = update.message
    current_state = state.get(user_id)
    if current_state is not None:
        if current_state in ('add_new_group_user', 'join_new_group_user'):
            result = new_user_process(
                update=update,
                context=context
            )
            if result is False:
                return

        state.pop(user_id, None)
    elif message.reply_to_message and message.text.lower() == 'delete':
        reply_message_id = message.reply_to_message.message_id
        delete_picture(reply_message_id=reply_message_id)
        context.bot.delete_message(
            chat_id=update.effective_chat.id,
            message_id=reply_message_id,
        )
        context.bot.delete_message(
            chat_id=update.effective_chat.id,
            message_id=message.message_id,
        )
    elif 'role ' in message.text.lower():
        change_role(
            update=update,
            message=message.text
        )
        questions_process(update=update, context=context)
    else:
        context.bot.delete_message(
            chat_id=update.effective_chat.id,
            message_id=message.message_id,
        )


def new_user_process(update: Update, context: CallbackContext) -> NoReturn:
    role = 'user'
    group_name = update.message.text.lower()
    chat_user = update.effective_chat
    user_id = chat_user.id
    current_state = state.get(user_id)
    if current_state == 'add_new_group_user':
        group_id = create_group(name=group_name)
        if group_id is False:
            update.message.reply_text(
                f'The group with name "{group_name}" already exists. Please, try again'
            )
            return False
        role = 'admin'
    elif current_state == 'join_new_group_user':
        group_id = get_group_by_name(name=group_name)
        if group_id is False:
            update.message.reply_text(f'Not found any group with name "{group_name}". Please, try again')
            return False

    else:
        group_id = 'default'
    new_user_data = {
        'user_id': user_id,
        'username': chat_user.username,
        'first_name': chat_user.first_name,
        'last_name': chat_user.last_name,
        'role': role,
        'group_id': group_id
    }
    create_new_user(user_data=new_user_data)
    admin_text = (
        ' as admin. Now you can upload questions to group and assign new admins \n\n'
        'You can use the following commands to manage your group: \n\n'
        '/continue: proceeds playing the game\n\n'
        'role <username> <rolename>: changes user role (available roles: admin, user)\n\n'
        'To upload an image just upload it in Telegram \n\n'
        'To remove an image just comment it with message "delete"'
    )
    text = admin_text if role == 'admin' else ''
    activity_number = update.message.message_id  # the highest number has the last active group
    assign_group_activity_number(
        user_id=user_id,
        activity_number=activity_number,
        group_id=group_id
    )
    update.message.reply_text(f'You\'ve joined to group "{group_name}"{text}')
    questions_process(update, context)


def change_role(update: Update, message: AnyStr) -> NoReturn:
    user_id = update.effective_user.id
    try:
        _, username, role = message.split()
    except ValueError:
        update.message.reply_text('Incorrect message format')
    else:
        result = change_user_role(user_id, username, role)
        update.message.reply_text(result)
