import sqlite3
from typing import NoReturn, Dict

from db.connection import connection


SELECT_USERS_GROUPS_DATA = (
    'SELECT name '
    'FROM groups '
    'INNER JOIN users '
    'ON users.telegram_user_id={user_id} '
    'AND users.group_id=groups.name'
)

SELECT_USER_BY_GROUP = (
    'SELECT telegram_user_id '
    'FROM users '
    'WHERE telegram_user_id={user_id} '
    'AND group_id="{group_id}"'
)

INSERT_NEW_USER = (
    'INSERT INTO users (telegram_user_id, username, first_name, last_name, role, group_id) '
    'VALUES ({user_id}, "{username}", "{first_name}", "{last_name}", "{role}", "{group_id}")'
)

SELECT_ANSWERS_BY_USER_GROUP = (
    'SELECT picture_id '
    'FROM picture_answers '
    'WHERE user_id={user_id} '
    'AND group_id="{group_id}"'
)

SELECT_NEWEST_QUESTIONS = (
    'SELECT id, picture_id '
    'FROM picture_questions '
    'WHERE id>{question_id} '
    'AND group_id="{group_id}"'
)

INSERT_ANSWER = (
    'INSERT INTO picture_answers (user_id, picture_id, group_id, answer) '
    'VALUES ({user_id}, {picture_id}, "{group_id}", {answer})'
)

INSERT_PICTURE = (
    'INSERT INTO picture_questions (picture_id, group_id, message_id) '
    'VALUES ("{picture_id}", "{group_id}", {message_id})'
)

INSERT_NEW_GROUP = (
    'INSERT INTO groups (name) '
    'VALUES ("{name}")'
)

SELECT_GROUP = (
    'SELECT name '
    'FROM groups '
    'WHERE name="{name}"'
)

SELECT_CURRENT_GROUP = (
    'SELECT group_id, MAX(group_activity_number), role '
    'FROM users '
    'WHERE telegram_user_id={user_id}'
)

UPDATE_ACTIVITY_NUMBER = (
    'UPDATE users '
    'SET group_activity_number={activity_number} '
    'WHERE telegram_user_id={user_id} '
    'AND group_id="{group_id}"'
)

DELETE_PICTURE = (
    'DELETE FROM picture_questions '
    'WHERE message_id={reply_message_id}'
)

SELECT_USERS_ANSWERS = (
    'SELECT user_id, picture_id, answer '
    'FROM picture_answers '
    'WHERE picture_answers.group_id="{group_id}"'
)

SELECT_USER_DETAILS = (
    'SELECT telegram_user_id, username, first_name, last_name '
    'FROM users '
    'WHERE group_id="{group_id}"'
)

UPDATE_USER_ROLE = (
    'UPDATE users '
    'SET role="{role}" '
    'WHERE username="{username}" '
    'AND group_id="{group_id}"'
)


def check_new_user_in_db(user_id: int) -> int:
    conn = connection()
    cursor = conn.cursor()
    cursor.execute(SELECT_USERS_GROUPS_DATA.format(user_id=user_id))
    groups = cursor.fetchall()
    conn.close()
    return groups


def create_new_user(user_data: Dict) -> NoReturn:
    conn = connection()
    cursor_get_user = conn.cursor()
    cursor_get_user.execute(
        SELECT_USER_BY_GROUP.format(
            user_id=user_data['user_id'],
            group_id=user_data['group_id']
        )
    )
    user = cursor_get_user.fetchone()
    if user:
        conn.close()
        return False

    query = INSERT_NEW_USER.format(
        user_id=user_data['user_id'],
        username=user_data['username'],
        first_name=user_data['first_name'],
        last_name=user_data['last_name'],
        role=user_data['role'],
        group_id=user_data['group_id']
    )
    cursor_add_user = conn.cursor()
    cursor_add_user.execute(query)
    conn.commit()
    conn.close()


def get_current_passed_question(user_id):
    conn = connection()
    group_id = get_group_id(user_id)[0]
    cursor = conn.cursor()
    query = SELECT_ANSWERS_BY_USER_GROUP.format(
        user_id=user_id,
        group_id=group_id
    )
    cursor.execute(query)
    res = cursor.fetchall() or [[0]]
    conn.close()
    return res[-1][0]


def get_questions(question_id, user_id):
    conn = connection()
    group_id = group_id_query(conn, user_id)[0]
    cursor = conn.cursor()
    query = SELECT_NEWEST_QUESTIONS.format(
        question_id=question_id,
        group_id=group_id,
    )
    cursor.execute(query)
    res = cursor.fetchone()
    conn.close()
    return res


def save_result_to_db(user_id, question_id, answer):
    conn = connection()
    group_id = get_group_id(user_id)[0]
    cursor = conn.cursor()
    query = INSERT_ANSWER.format(
        user_id=user_id,
        picture_id=question_id,
        group_id=group_id,
        answer=answer
    )
    cursor.execute(query)
    conn.commit()
    conn.close()


def add_picture(picture_id, user_id, message_id):
    conn = connection()
    user_group = group_id_query(conn, user_id)
    if user_group[2] != 'admin':
        conn.close()
        return False

    cursor = conn.cursor()
    query = INSERT_PICTURE.format(
        picture_id=picture_id,
        group_id=user_group[0],
        message_id=message_id
    )
    cursor.execute(query)
    conn.commit()
    conn.close()


def create_group(name):
    conn = connection()
    cursor = conn.cursor()
    query = INSERT_NEW_GROUP.format(name=name)
    try:
        cursor.execute(query)
    except sqlite3.IntegrityError:
        return False
    conn.commit()
    conn.close()
    return name


def get_group_by_name(name):
    conn = connection()
    cursor = conn.cursor()
    query = SELECT_GROUP.format(name=name)
    cursor.execute(query)
    res = cursor.fetchone() or [False]
    conn.close()
    return res[0]


def assign_group_activity_number(user_id, activity_number, group_id):
    conn = connection()
    cursor = conn.cursor()
    query = UPDATE_ACTIVITY_NUMBER.format(
        activity_number=activity_number,
        user_id=user_id,
        group_id=group_id
    )
    cursor.execute(query)
    conn.commit()
    conn.close()


def group_id_query(conneection, user_id):
    group_id_cursor = conneection.cursor()
    group_id_cursor.execute(SELECT_CURRENT_GROUP.format(user_id=user_id))
    return group_id_cursor.fetchone()


def get_group_id(user_id):
    conn = connection()
    data = group_id_query(conn, user_id)
    conn.close()
    return data


def delete_picture(reply_message_id):
    conn = connection()
    cursor = conn.cursor()
    query = DELETE_PICTURE.format(reply_message_id=reply_message_id)
    cursor.execute(query)
    conn.commit()
    conn.close()


def get_results_by_group(user_id):
    conn = connection()
    group_id = group_id_query(conn, user_id)[0]
    cursor_answers = conn.cursor()
    query_answers = SELECT_USERS_ANSWERS.format(group_id=group_id)
    cursor_answers.execute(query_answers)
    answers = cursor_answers.fetchall()
    cursor_users = conn.cursor()
    query_users = SELECT_USER_DETAILS.format(group_id=group_id)
    cursor_users.execute(query_users)
    users = cursor_users.fetchall()
    conn.close()
    return {'users': users, 'answers': answers}


def change_user_role(user_id, target_username, role):
    conn = connection()
    user_group = group_id_query(conn, user_id)
    if user_group[2] != 'admin':
        conn.close()
        return 'You are not admin to perform this action'

    if role not in ('user', 'admin'):
        return f'incorrect role: "{role}"'

    cursor = conn.cursor()
    query = UPDATE_USER_ROLE.format(
        role=role,
        username=target_username,
        group_id=user_group[0]
    )
    cursor.execute(query)
    conn.commit()
    conn.close()
    if cursor.rowcount:
        return f'The role of user "{target_username}" is changed to "{role}"'
    return f'Cannot change the role of user "{target_username}" to "{role}". Please check username'
