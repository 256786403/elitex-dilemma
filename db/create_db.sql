create table groups
(
    id   INTEGER
        constraint groups_pk
            primary key autoincrement,
    name TEXT
);


create table picture_questions
(
    id         INTEGER
        constraint picture_questions_pk
            primary key autoincrement,
    picture_id TEXT,
    group_id   INTEGER default 1
        references groups
            on update cascade on delete cascade,
    message_id INTEGER
);

create unique index picture_questions_message_id_uindex
    on picture_questions (message_id);

create unique index picture_questions_telegram_id_uindex
    on picture_questions (id);



create table users
(
    telegram_user_id      INTEGER
        references groups
            on update cascade on delete cascade,
    username              TEXT,
    first_name            TEXT,
    last_name             TEXT,
    role                  TEXT    default 'user',
    group_id              TEXT,
    group_activity_number INTEGER default 1 not null,
    id                    INTEGER
        constraint users_pk
            primary key
);



create table picture_answers
(
    user_id    INTEGER
        references users
            on update cascade on delete cascade,
    picture_id INTEGER
        references picture_questions
            on update cascade on delete cascade,
    group_id   TEXT
        references groups
            on update cascade on delete cascade,
    answer     TEXT
);


insert into  groups (name) values ('default')
