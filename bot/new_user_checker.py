from typing import NoReturn

from telegram import (
    Update,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)

from db.operate import check_new_user_in_db


def new_user_group_choice(update: Update) -> NoReturn:
    message = update.message
    groups = check_new_user_in_db(user_id=update.effective_chat.id)
    keyboard = [
        [
            InlineKeyboardButton('Create new group', callback_data='create_new_group'),
            InlineKeyboardButton('Join new group', callback_data='join_new_group'),
        ]
    ]
    if 1 not in [g[0] for g in groups]:
        keyboard.append(
            [
                InlineKeyboardButton('Join default group', callback_data='join_default_group')
            ]
        )
    if groups:
        keyboard.extend([
            [
                InlineKeyboardButton(
                    f'Back to group {group[0]}',
                    callback_data=f'back_to_group_#{group[0]}'
                )
            ] for group in groups
        ])
    reply_markup = InlineKeyboardMarkup(keyboard)
    msg_part = 'back' if groups else f'to {update.effective_chat.bot.name}'
    message.reply_text(
        f'Welcome {msg_part}! Please, join or create a group',
        reply_markup=reply_markup
    )
