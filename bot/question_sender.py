from typing import NoReturn, AnyStr

from telegram import (
    Update,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from telegram.ext import CallbackContext

from bot.result_sender import get_result
from db.operate import (
    get_current_passed_question,
    get_questions,
)


def questions_process(update: Update, context: CallbackContext) -> NoReturn:
    user_id = update.effective_user.id
    current_passed_question = get_current_passed_question(
        user_id=user_id
    )
    question_data = get_questions(
        question_id=current_passed_question,
        user_id=user_id
    )
    if question_data:
        question_id, url = question_data
        if int(question_id) and int(question_id) % 10 == 0:
            get_result(update=update)
        send_question(
            update=update,
            question_id=question_id,
            url=url,
            context=context
        )
    else:
        check_new_questions_existing(
            update=update,
            context=context,
        )


def send_question(update: Update, question_id: int, url: AnyStr, context: CallbackContext) -> NoReturn:
    message = update.message or update.callback_query.message
    keyboard = [
        [
            InlineKeyboardButton('⟸', callback_data=f'answer_{question_id}_1'),
            InlineKeyboardButton('⟹', callback_data=f'answer_{question_id}_2')
        ],
        [
            InlineKeyboardButton('Don\'t prefer anything', callback_data=f'answer_{question_id}_0')
        ],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    context.bot.sendPhoto(
        chat_id=message.chat.id,
        photo=url,
        reply_markup=reply_markup
    )


def check_new_questions_existing(update: Update, context: CallbackContext) -> NoReturn:
    message = update.message or update.callback_query.message
    keyboard = [
        [
            InlineKeyboardButton(
                '⤋ Get new questions ⤋',
                callback_data='check_new_questions_exist'
            ),
            InlineKeyboardButton(
                '⤋ Get result ⤋',
                callback_data='get_result'
            ),
        ],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    message.reply_text(' ⤋ ' * 18, reply_markup=reply_markup)
    if update.callback_query:
        context.bot.answer_callback_query(
            callback_query_id=update.callback_query.id,
            text='Cannot find anything for you yet. Please, try later'
        )
