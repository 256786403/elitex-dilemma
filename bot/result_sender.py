from collections import defaultdict
from typing import NoReturn, Tuple, Dict, List

from telegram import Update

from db.operate import get_results_by_group
from copy import deepcopy


def get_result(update: Update) -> NoReturn:
    message = update.message or update.callback_query.message
    user_id = update.effective_user.id
    optimized_answers, optimized_userdata = optimize_result(
        user_id=user_id
    )
    message_data = make_message(
        user_id=user_id,
        optimized_answers=optimized_answers,
        optimized_userdata=optimized_userdata
    )
    if message_data:
        h_line = '-' * 60 + '\n'
        message.reply_text(
            h_line + '--------------\n'.join(message_data) + h_line
        )


def optimize_result(user_id: int) -> Tuple:
    optimized_answers = defaultdict(dict)
    optimized_userdata = {}
    results = get_results_by_group(user_id=user_id)
    answers = results['answers']
    users = results['users']
    for db_user_id, picture_id, answer in answers:
        optimized_answers[db_user_id][picture_id] = answer
    for db_user_id, username, first_name, last_name in users:
        username = f' (@{username})' if username != 'None' else ''
        first_name = first_name if first_name != 'None' else ''
        last_name = last_name if last_name != 'None' else ''
        optimized_userdata[db_user_id] = f'{first_name} {last_name}{username}'
    return optimized_answers, optimized_userdata


def get_matched_questions(optimized_answers: Dict, user_id: int) -> Tuple:
    current_user_data = optimized_answers.pop(user_id, {})
    statistic = {k: 0 for k in optimized_answers.keys()}
    matched_questions = defaultdict(int)
    for question, answer in current_user_data.items():
        for compare_user_id in optimized_answers:
            compare_user_answer = optimized_answers[compare_user_id].get(question)
            if compare_user_answer:
                if compare_user_answer == answer:
                    statistic[compare_user_id] += 1
                matched_questions[compare_user_id] += 1
    return matched_questions, statistic, len(current_user_data)


def make_message(user_id: int, optimized_answers: Dict, optimized_userdata: Dict) -> List:
    matched_questions, statistic, passed_questions_len = get_matched_questions(optimized_answers, user_id)
    if passed_questions_len < 10:
        return []

    message_data = []
    if statistic:
        optimized_answers.pop(user_id, None)
        protogonist = get_protogonist(matched_questions, statistic)
        ontogonist = get_ontogonist(matched_questions, statistic)

        if protogonist and statistic[protogonist] / matched_questions[protogonist] > 0.5:
            p_out_of = f'{statistic[protogonist]} out of {matched_questions[protogonist]} common answers\n'
            message_data.append(f'Your protogonist: \n {optimized_userdata[protogonist]} \n {p_out_of}')
        if ontogonist and statistic[ontogonist] / matched_questions[ontogonist] <= 0.5:
            o_out_of = f'{statistic[ontogonist]} out of {matched_questions[ontogonist]} common answers\n'
            message_data.append(f'Your ontogonist: \n {optimized_userdata[ontogonist]} \n {o_out_of}')
    return message_data


def get_protogonist(matched_questions, statistic):
    statistic = deepcopy(statistic)
    matched_questions = deepcopy(matched_questions)
    while statistic:
        possible_protogonist = max(statistic, key=statistic.get)
        statistic.pop(possible_protogonist)
        m = matched_questions.pop(possible_protogonist, 0)
        print(34343434, m)
        if m > 9:
            return possible_protogonist


def get_ontogonist(matched_questions, statistic):
    statistic = deepcopy(statistic)
    matched_questions = deepcopy(matched_questions)
    while statistic:
        possible_ontoogonist = min(statistic, key=statistic.get)
        statistic.pop(possible_ontoogonist)
        m = matched_questions.pop(possible_ontoogonist, 0)
        print(12121212, m)
        if m > 9:
            return possible_ontoogonist
