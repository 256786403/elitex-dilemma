
source root
export PYTHONPATH="${PYTHONPATH}:/elitex-dilemma"

install requirements:
pip install requirements.txt

run project:
python bot/dispatchers.py

if needed to recreate database run command:
export python db/create.py
