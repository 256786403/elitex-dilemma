from db.connection import connection


conn = connection()
cursor = conn.cursor()


sql_file = open("db/create_db.sql")
sql_as_string = sql_file.read()
cursor.executescript(sql_as_string)
