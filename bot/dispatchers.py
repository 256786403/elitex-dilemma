from telegram.ext import (
    Updater,
    MessageHandler,
    CallbackQueryHandler,
    Filters,
    CommandHandler,
)

from bot.conversation_button import conversation_router_button
from bot.conversation_commands import continue_command, start
from bot.conversation_picture import conversation_router_picture
from bot.conversation_text import conversation_router_text

# token for ELITEX dilemma bot
TOKEN = '1203595184:AAEMSZKpNaCkQEOlIBOOcUVfxB6391z47kk'


updater = Updater(TOKEN)

dp = updater.dispatcher
dp.add_handler(CommandHandler('start', start))
dp.add_handler(CommandHandler('continue', continue_command))


button_handler = CallbackQueryHandler(conversation_router_button)
text_handler = MessageHandler(Filters.text, conversation_router_text)
picture_handler = MessageHandler(Filters.photo, conversation_router_picture)


dp.add_handler(button_handler)
dp.add_handler(text_handler)
dp.add_handler(picture_handler)


updater.start_polling()
updater.idle()
